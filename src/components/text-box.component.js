//Dynamic Attribute
import React from "react";
const TextBox = (props) => (
  <input
    type="text"
    required
    className={props.attribute1.class}
    value={props.attribute1.username}
    onChange={props.onChangeUsername}
    size={props.attribute1.size}
    id={props.attribute1.id}
  />
);

export default TextBox;
