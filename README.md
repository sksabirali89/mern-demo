Code for MERN Demo.

## Installation steps

1. Clone the Project

2. **cd mern-demo** and install forntend dependencies **npm install**

3. **cd backend** and install dependencies **npm install**

4. Create .env file inside backend directory **cp .env-sample ./.env**
   and replace credentials

5. Start backend server **node server.js**

6. Start frontend service **npm start**

Navigate to browser http://localhost:3000/
